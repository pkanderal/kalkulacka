﻿using System;
using System.Windows.Forms;

namespace Kalkulacka
{
    public partial class KalkulačkaForm : Form
    {
        double hodnota = 0;  //číslo a
        string operace = "";  // + - * /
        bool operator_kliknut = false;  //klikl jsi na operátor
        bool masVysledek = false; // (ne)máš na kalkulačce výsledek počítání

        public KalkulačkaForm() //formulář
        {
            InitializeComponent();
        }

        public void Button_Click(object sender, EventArgs e)
        {
            if (/*(vysledekTextBox1.Text == "0") || */(operator_kliknut) || (masVysledek == true))
                //TODO: prida nulu pred desetinnou carku, ale i pred cisla, kdyz zrusim comment, jsou cisla s "," bez nuly na zacatku
                vysledekTextBox1.Clear();

            masVysledek = false;
            operator_kliknut = false;
            Button b = (Button)sender;

            if (b.Text == ",")
            {
                if (!vysledekTextBox1.Text.Contains(","))  //jenom jedna desetinná čárka
                    vysledekTextBox1.Text = vysledekTextBox1.Text + b.Text;
            }
            else if (b.Text == "," && vysledekTextBox1.Text.Length==2)
            {
                vysledekTextBox1.Text = "0," + b.Text;
            }
            else
                vysledekTextBox1.Text = vysledekTextBox1.Text + b.Text;
        }

        private void ClearButton_Click(object sender, EventArgs e) //tlačítko C
        {
            vysledekTextBox1.Text = "0";
            hodnota = 0;
        }

        private void Operator_Click(object sender, EventArgs e) //kliknutí na operátor
        {
            Button b = (Button)sender;
            if (hodnota != 0)
            {
                rovnaSeButton.PerformClick();
                operator_kliknut = true;
                operace = b.Text;
                rovniceLabel.Text = hodnota + " " + operace;
            }
            operace = b.Text;
            if (!(vysledekTextBox1.Text == "," || vysledekTextBox1.Text == ""))
            {
                hodnota = Double.Parse(vysledekTextBox1.Text);
            }
            else
            {
                hodnota = 0;
            }

            operator_kliknut = true;
            rovniceLabel.Text = hodnota + " " + operace;

        }

        public void RovnaSeButton_Click(object sender, EventArgs e)
        {
            string vysledek = "";
            if ((vysledekTextBox1.Text == ","))
            {
                hodnota = 0;
            }
            else if ((vysledekTextBox1.Text == ""))
            {
                hodnota = 0;
            }
            else
            {
                switch (operace)
                {
                    case "+":
                        vysledek = (hodnota + double.Parse(vysledekTextBox1.Text)).ToString();
                        break;
                    case "-":
                        vysledek = (hodnota - double.Parse(vysledekTextBox1.Text)).ToString();
                        break;
                    case "/":
                        if (vysledekTextBox1.Text == "0")
                        {
                            MessageBox.Show("Nulou nelze dělit",
                            "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            historieListBox1.Items.Add("Dělení nulou.");
                            operace = "";
                        }
                        else
                        {
                            vysledek = (hodnota / double.Parse(vysledekTextBox1.Text)).ToString();
                        }
                        break;
                    case "*":
                        vysledek = (hodnota * double.Parse(vysledekTextBox1.Text)).ToString();
                        break;
                    default:
                        break;

                }//konec switche
            }
            if (!(vysledekTextBox1.Text == ","))
            {
                hodnota = 0;
            }
            else
            {
                MessageBox.Show("Ještě jsi nic nespočítal.",
                        "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            operace = "";
            if (vysledekTextBox1.Text == "")
            {
                MessageBox.Show("Ještě jsi nic nespočítal.",
                        "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                historieListBox1.Items.Add(rovniceLabel.Text + " " + vysledekTextBox1.Text.ToString() + " = " + vysledek);
            }

            historieListBox1.SelectedIndex = historieListBox1.Items.Count - 1;

            vysledekTextBox1.Text = vysledek;
            vysledek = "";
            rovniceLabel.Text = "";
            hodnota = 0;
            masVysledek = true;
        }

        private void KonecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e) //psaní klávesnicí
        {
            switch (e.KeyChar.ToString())
            {
                case "1":
                    jednaButton.PerformClick();
                    break;
                case "2":
                    dvaButton.PerformClick();
                    break;
                case "3":
                    triButton.PerformClick();
                    break;
                case "4":
                    ctyriButton.PerformClick();
                    break;
                case "5":
                    petButton.PerformClick();
                    break;
                case "6":
                    sestButton.PerformClick();
                    break;
                case "7":
                    sedmButton.PerformClick();
                    break;
                case "8":
                    osmButton.PerformClick();
                    break;
                case "9":
                    devetButton.PerformClick();
                    break;
                case "0":
                    nulaButton.PerformClick();
                    break;
                case ",":
                    carkaButton.PerformClick();
                    break;
                case "-":
                    minusButton.PerformClick();
                    break;
                case "+":
                    plusButton.PerformClick();
                    break;
                case "/":
                    delenoButton.PerformClick();
                    break;
                case "*":
                    kratButton.PerformClick();
                    break;
                case ".": //"desetinná tečka"
                    carkaButton.PerformClick();
                    break;
                case "ENTER":
                    rovnaSeButton.PerformClick();
                    break;
                default:
                    break;
            }
        }

        private void OAplikaciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            aboutForm about = new aboutForm();
            about.Show();
        }


    }
}
