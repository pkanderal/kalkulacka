﻿using System;
using System.Windows.Forms;

namespace Kalkulacka
{
    public partial class Form1 : Form
        

    {
        double hodnota = 0;  //číslo a
        string operace = "";  // + - * /
        bool operator_kliknut=false;  //klikl jsi na operátor
        bool masVysledek = false; // máš na kalkulačce výsledek počítání

        public Form1() //formulář
        {
            InitializeComponent();
        }

        public void button_Click(object sender, EventArgs e)
        {
            if ((vysledekTextBox1.Text == "0") || (operator_kliknut) || (masVysledek==true))
                vysledekTextBox1.Clear();

            masVysledek = false;
            operator_kliknut = false;
            Button b = (Button)sender;
            if (b.Text == ",")
            {
                if (!vysledekTextBox1.Text.Contains(","))  //jenom jedna desetinná čárka
                    vysledekTextBox1.Text = vysledekTextBox1.Text + b.Text;
                
            }
            else
                vysledekTextBox1.Text = vysledekTextBox1.Text + b.Text;
                               
        }

        private void clearButton_Click(object sender, EventArgs e) //tlačítko C
        {
            vysledekTextBox1.Text="0";
            hodnota = 0;
        }

        private void operator_Click(object sender, EventArgs e) //kliknutí na operátor
        {
            Button b = (Button)sender;
            if (hodnota!=0)
            {
                rovnaSeButton.PerformClick();
                operator_kliknut = true;
                operace = b.Text;
                rovnice.Text = hodnota + " " + operace;
            }
            else
            {

            }
            operace = b.Text;
            hodnota = Double.Parse(vysledekTextBox1.Text);
            operator_kliknut = true;
            rovnice.Text = hodnota +" "+ operace;

        }

        public void rovnaSeButton_Click(object sender, EventArgs e)
        {
            string vysledek="";
            switch (operace)
            {
                case "+":
                    vysledek = (hodnota + double.Parse(vysledekTextBox1.Text)).ToString();
                    break;
                case "-":
                    vysledek = (hodnota - double.Parse(vysledekTextBox1.Text)).ToString();
                    break;
                case "/":
                    if (vysledekTextBox1.Text == "0")
                    {
                        //Console.WriteLine("");
                        MessageBox.Show("Nulou nelze dělit",
                        "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        historieListBox1.Items.Add("Dělení nulou.");
                        
                    }
                    else
                    {
                        vysledek = (hodnota / double.Parse(vysledekTextBox1.Text)).ToString();
                    }
                    break;
                case "*":
                    vysledek = (hodnota * double.Parse(vysledekTextBox1.Text)).ToString();
                    break;
                default:
                    break;
                    
            }//konec switche

            hodnota = double.Parse(vysledekTextBox1.Text);
            operace = "";
            if (vysledekTextBox1.Text == "")
            {
                MessageBox.Show("Ještě jsi nic nespočítal.",
                        "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Console.WriteLine("Ještě jsi nic nespočítal.");
            }
            else
            {
                historieListBox1.Items.Add(rovnice.Text +" "+ vysledekTextBox1.Text.ToString() + " = " + vysledek);
            }

            historieListBox1.SelectedIndex = historieListBox1.Items.Count - 1;

            vysledekTextBox1.Text = vysledek;
            vysledek = "";
            rovnice.Text = "";
            hodnota = 0;
           masVysledek = true;


        }

        private void konecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar.ToString())
            {
                case "1":
                    jednaButton.PerformClick();
                    break;
                case "2":
                    dvaButton.PerformClick();
                    break;
                case "3":
                    triButton.PerformClick();
                    break;
                case "4":
                    ctyriButton.PerformClick();
                    break;
                case "5":
                    petButton.PerformClick();
                    break;
                case "6":
                    sestButton.PerformClick();
                    break;
                case "7":
                    sedmButton.PerformClick();
                    break;
                case "8":
                    osmButton.PerformClick();
                    break;
                case "9":
                    devetButton.PerformClick();
                    break;
                case "0":
                    nulaButton.PerformClick();
                    break;
                case ",":
                    carkaButton.PerformClick();
                    break;
                case "-":
                    minusButton.PerformClick();
                    break;
                case "+":
                    plusButton.PerformClick();
                    break;
                case "/":
                    delenoButton.PerformClick();
                    break;
                case "*":
                    kratButton.PerformClick();
                    break;
                case "ENTER":
                    rovnaSeButton.PerformClick();
                    break;
                default:
                    break;
            }
        }

        private void oAplikaciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 about = new Form2();
            about.Show();

        }
               
    }
}
