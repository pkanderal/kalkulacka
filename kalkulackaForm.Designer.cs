﻿namespace Kalkulacka
{
    partial class KalkulačkaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.historieListBox1 = new System.Windows.Forms.ListBox();
            this.vysledekTextBox1 = new System.Windows.Forms.TextBox();
            this.sedmButton = new System.Windows.Forms.Button();
            this.devetButton = new System.Windows.Forms.Button();
            this.osmButton = new System.Windows.Forms.Button();
            this.petButton = new System.Windows.Forms.Button();
            this.sestButton = new System.Windows.Forms.Button();
            this.plusButton = new System.Windows.Forms.Button();
            this.ctyriButton = new System.Windows.Forms.Button();
            this.dvaButton = new System.Windows.Forms.Button();
            this.triButton = new System.Windows.Forms.Button();
            this.jednaButton = new System.Windows.Forms.Button();
            this.carkaButton = new System.Windows.Forms.Button();
            this.rovnaSeButton = new System.Windows.Forms.Button();
            this.nulaButton = new System.Windows.Forms.Button();
            this.delenoButton = new System.Windows.Forms.Button();
            this.kratButton = new System.Windows.Forms.Button();
            this.minusButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.konecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oAplikaciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rovniceLabel = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // historieListBox1
            // 
            this.historieListBox1.FormattingEnabled = true;
            this.historieListBox1.Location = new System.Drawing.Point(12, 36);
            this.historieListBox1.Name = "historieListBox1";
            this.historieListBox1.Size = new System.Drawing.Size(219, 95);
            this.historieListBox1.TabIndex = 0;
            this.historieListBox1.TabStop = false;
            // 
            // vysledekTextBox1
            // 
            this.vysledekTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.vysledekTextBox1.Location = new System.Drawing.Point(13, 138);
            this.vysledekTextBox1.Name = "vysledekTextBox1";
            this.vysledekTextBox1.Size = new System.Drawing.Size(218, 29);
            this.vysledekTextBox1.TabIndex = 1;
            this.vysledekTextBox1.TabStop = false;
            this.vysledekTextBox1.Text = "0";
            this.vysledekTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // sedmButton
            // 
            this.sedmButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sedmButton.Location = new System.Drawing.Point(12, 230);
            this.sedmButton.Name = "sedmButton";
            this.sedmButton.Size = new System.Drawing.Size(50, 50);
            this.sedmButton.TabIndex = 2;
            this.sedmButton.TabStop = false;
            this.sedmButton.Text = "7";
            this.sedmButton.UseVisualStyleBackColor = true;
            this.sedmButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // devetButton
            // 
            this.devetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.devetButton.Location = new System.Drawing.Point(124, 230);
            this.devetButton.Name = "devetButton";
            this.devetButton.Size = new System.Drawing.Size(50, 50);
            this.devetButton.TabIndex = 4;
            this.devetButton.TabStop = false;
            this.devetButton.Text = "9";
            this.devetButton.UseVisualStyleBackColor = true;
            this.devetButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // osmButton
            // 
            this.osmButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.osmButton.Location = new System.Drawing.Point(68, 230);
            this.osmButton.Name = "osmButton";
            this.osmButton.Size = new System.Drawing.Size(50, 50);
            this.osmButton.TabIndex = 5;
            this.osmButton.TabStop = false;
            this.osmButton.Text = "8";
            this.osmButton.UseVisualStyleBackColor = true;
            this.osmButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // petButton
            // 
            this.petButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.petButton.Location = new System.Drawing.Point(68, 286);
            this.petButton.Name = "petButton";
            this.petButton.Size = new System.Drawing.Size(50, 50);
            this.petButton.TabIndex = 9;
            this.petButton.TabStop = false;
            this.petButton.Text = "5";
            this.petButton.UseVisualStyleBackColor = true;
            this.petButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // sestButton
            // 
            this.sestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sestButton.Location = new System.Drawing.Point(124, 286);
            this.sestButton.Name = "sestButton";
            this.sestButton.Size = new System.Drawing.Size(50, 50);
            this.sestButton.TabIndex = 8;
            this.sestButton.TabStop = false;
            this.sestButton.Text = "6";
            this.sestButton.UseVisualStyleBackColor = true;
            this.sestButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // plusButton
            // 
            this.plusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.plusButton.Location = new System.Drawing.Point(180, 342);
            this.plusButton.Name = "plusButton";
            this.plusButton.Size = new System.Drawing.Size(50, 106);
            this.plusButton.TabIndex = 7;
            this.plusButton.TabStop = false;
            this.plusButton.Text = "+";
            this.plusButton.UseVisualStyleBackColor = true;
            this.plusButton.Click += new System.EventHandler(this.Operator_Click);
            // 
            // ctyriButton
            // 
            this.ctyriButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ctyriButton.Location = new System.Drawing.Point(12, 286);
            this.ctyriButton.Name = "ctyriButton";
            this.ctyriButton.Size = new System.Drawing.Size(50, 50);
            this.ctyriButton.TabIndex = 6;
            this.ctyriButton.TabStop = false;
            this.ctyriButton.Text = "4";
            this.ctyriButton.UseVisualStyleBackColor = true;
            this.ctyriButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // dvaButton
            // 
            this.dvaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dvaButton.Location = new System.Drawing.Point(68, 342);
            this.dvaButton.Name = "dvaButton";
            this.dvaButton.Size = new System.Drawing.Size(50, 50);
            this.dvaButton.TabIndex = 13;
            this.dvaButton.TabStop = false;
            this.dvaButton.Text = "2";
            this.dvaButton.UseVisualStyleBackColor = true;
            this.dvaButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // triButton
            // 
            this.triButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.triButton.Location = new System.Drawing.Point(124, 342);
            this.triButton.Name = "triButton";
            this.triButton.Size = new System.Drawing.Size(50, 50);
            this.triButton.TabIndex = 12;
            this.triButton.TabStop = false;
            this.triButton.Text = "3";
            this.triButton.UseVisualStyleBackColor = true;
            this.triButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // jednaButton
            // 
            this.jednaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.jednaButton.Location = new System.Drawing.Point(12, 342);
            this.jednaButton.Name = "jednaButton";
            this.jednaButton.Size = new System.Drawing.Size(50, 50);
            this.jednaButton.TabIndex = 10;
            this.jednaButton.TabStop = false;
            this.jednaButton.Text = "1";
            this.jednaButton.UseVisualStyleBackColor = true;
            this.jednaButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // carkaButton
            // 
            this.carkaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.carkaButton.Location = new System.Drawing.Point(68, 398);
            this.carkaButton.Name = "carkaButton";
            this.carkaButton.Size = new System.Drawing.Size(50, 50);
            this.carkaButton.TabIndex = 16;
            this.carkaButton.TabStop = false;
            this.carkaButton.Text = ",";
            this.carkaButton.UseVisualStyleBackColor = true;
            this.carkaButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // rovnaSeButton
            // 
            this.rovnaSeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rovnaSeButton.Location = new System.Drawing.Point(124, 396);
            this.rovnaSeButton.Name = "rovnaSeButton";
            this.rovnaSeButton.Size = new System.Drawing.Size(50, 50);
            this.rovnaSeButton.TabIndex = 0;
            this.rovnaSeButton.Text = "=";
            this.rovnaSeButton.UseVisualStyleBackColor = true;
            this.rovnaSeButton.Click += new System.EventHandler(this.RovnaSeButton_Click);
            // 
            // nulaButton
            // 
            this.nulaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nulaButton.Location = new System.Drawing.Point(12, 398);
            this.nulaButton.Name = "nulaButton";
            this.nulaButton.Size = new System.Drawing.Size(50, 50);
            this.nulaButton.TabIndex = 14;
            this.nulaButton.TabStop = false;
            this.nulaButton.Text = "0";
            this.nulaButton.UseVisualStyleBackColor = true;
            this.nulaButton.Click += new System.EventHandler(this.Button_Click);
            // 
            // delenoButton
            // 
            this.delenoButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.delenoButton.Location = new System.Drawing.Point(180, 174);
            this.delenoButton.Name = "delenoButton";
            this.delenoButton.Size = new System.Drawing.Size(50, 50);
            this.delenoButton.TabIndex = 20;
            this.delenoButton.TabStop = false;
            this.delenoButton.Text = "/";
            this.delenoButton.UseVisualStyleBackColor = true;
            this.delenoButton.Click += new System.EventHandler(this.Operator_Click);
            // 
            // kratButton
            // 
            this.kratButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kratButton.Location = new System.Drawing.Point(180, 230);
            this.kratButton.Name = "kratButton";
            this.kratButton.Size = new System.Drawing.Size(50, 50);
            this.kratButton.TabIndex = 19;
            this.kratButton.TabStop = false;
            this.kratButton.Text = "*";
            this.kratButton.UseVisualStyleBackColor = true;
            this.kratButton.Click += new System.EventHandler(this.Operator_Click);
            // 
            // minusButton
            // 
            this.minusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.minusButton.Location = new System.Drawing.Point(180, 286);
            this.minusButton.Name = "minusButton";
            this.minusButton.Size = new System.Drawing.Size(50, 50);
            this.minusButton.TabIndex = 18;
            this.minusButton.TabStop = false;
            this.minusButton.Text = "-";
            this.minusButton.UseVisualStyleBackColor = true;
            this.minusButton.Click += new System.EventHandler(this.Operator_Click);
            // 
            // clearButton
            // 
            this.clearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.clearButton.Location = new System.Drawing.Point(12, 173);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(162, 50);
            this.clearButton.TabIndex = 17;
            this.clearButton.TabStop = false;
            this.clearButton.Text = "C";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.souborToolStripMenuItem,
            this.oAplikaciToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(248, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // souborToolStripMenuItem
            // 
            this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.konecToolStripMenuItem});
            this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            this.souborToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.souborToolStripMenuItem.Text = "Soubor";
            // 
            // konecToolStripMenuItem
            // 
            this.konecToolStripMenuItem.Name = "konecToolStripMenuItem";
            this.konecToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.konecToolStripMenuItem.Text = "Konec";
            this.konecToolStripMenuItem.Click += new System.EventHandler(this.KonecToolStripMenuItem_Click);
            // 
            // oAplikaciToolStripMenuItem
            // 
            this.oAplikaciToolStripMenuItem.Name = "oAplikaciToolStripMenuItem";
            this.oAplikaciToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.oAplikaciToolStripMenuItem.Text = "O aplikaci";
            this.oAplikaciToolStripMenuItem.Click += new System.EventHandler(this.OAplikaciToolStripMenuItem_Click);
            // 
            // rovnice
            // 
            this.rovniceLabel.AutoSize = true;
            this.rovniceLabel.BackColor = System.Drawing.SystemColors.Window;
            this.rovniceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rovniceLabel.Location = new System.Drawing.Point(15, 143);
            this.rovniceLabel.Name = "rovnice";
            this.rovniceLabel.Size = new System.Drawing.Size(0, 20);
            this.rovniceLabel.TabIndex = 22;
            // 
            // kalkulackaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 457);
            this.Controls.Add(this.rovniceLabel);
            this.Controls.Add(this.delenoButton);
            this.Controls.Add(this.kratButton);
            this.Controls.Add(this.minusButton);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.carkaButton);
            this.Controls.Add(this.rovnaSeButton);
            this.Controls.Add(this.nulaButton);
            this.Controls.Add(this.dvaButton);
            this.Controls.Add(this.triButton);
            this.Controls.Add(this.jednaButton);
            this.Controls.Add(this.petButton);
            this.Controls.Add(this.sestButton);
            this.Controls.Add(this.plusButton);
            this.Controls.Add(this.ctyriButton);
            this.Controls.Add(this.osmButton);
            this.Controls.Add(this.devetButton);
            this.Controls.Add(this.sedmButton);
            this.Controls.Add(this.vysledekTextBox1);
            this.Controls.Add(this.historieListBox1);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(264, 496);
            this.MinimumSize = new System.Drawing.Size(264, 496);
            this.Name = "kalkulackaForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kalkulačka";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox historieListBox1;
        private System.Windows.Forms.TextBox vysledekTextBox1;
        private System.Windows.Forms.Button sedmButton;
        private System.Windows.Forms.Button devetButton;
        private System.Windows.Forms.Button osmButton;
        private System.Windows.Forms.Button petButton;
        private System.Windows.Forms.Button sestButton;
        private System.Windows.Forms.Button plusButton;
        private System.Windows.Forms.Button ctyriButton;
        private System.Windows.Forms.Button dvaButton;
        private System.Windows.Forms.Button triButton;
        private System.Windows.Forms.Button jednaButton;
        private System.Windows.Forms.Button carkaButton;
        private System.Windows.Forms.Button rovnaSeButton;
        private System.Windows.Forms.Button nulaButton;
        private System.Windows.Forms.Button delenoButton;
        private System.Windows.Forms.Button kratButton;
        private System.Windows.Forms.Button minusButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem konecToolStripMenuItem;
        private System.Windows.Forms.Label rovniceLabel;
        private System.Windows.Forms.ToolStripMenuItem oAplikaciToolStripMenuItem;
    }
}

